package kavi.vsi.backend.repository;

import kavi.vsi.backend.entity.User;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;


/**
 * 用户表存储过程
 * */
public interface UserRepository extends R2dbcRepository<User, Long> {

    @Query("SELECT * FROM " + User.tableName )
    Flux<User> findByUses();
}