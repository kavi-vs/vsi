package kavi.vsi.backend.properties

import com.fasterxml.jackson.annotation.JsonIgnore
import io.vertx.core.VertxOptions
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Vertx属性配置
 * */
@ConfigurationProperties(prefix = VertxProperties.PREFIX, ignoreUnknownFields = true)
open class VertxProperties (
        @JsonIgnore open var clusterType: String? = null, // 集群类型
        @JsonIgnore open var vertxTimeout: Long = 10      // 启动阻塞等待超时时间(秒)
) : VertxOptions() {

    companion object{
        const val PREFIX = "vertx"
    }
}