package kavi.vsi.backend

import io.vertx.core.VertxOptions
import io.vertx.core.json.JsonObject
import io.vertx.reactivex.core.Vertx
import kavi.vsi.backend.properties.VertxProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import kotlin.jvm.Throws
import io.vertx.core.spi.cluster.ClusterManager
import io.vertx.reactivex.core.eventbus.EventBus
import kavi.vsi.backend.utils.LoadFiles
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Configuration
@EnableConfigurationProperties(VertxProperties::class)
class BeanConfiguration {
    private val log : Logger = LoggerFactory.getLogger(this::class.java)


    /**
     * Vertx 服務初始化
     * */
    @Bean
    @Throws(Exception::class)
    fun vertx(properties: VertxProperties): Vertx {
        val clusterType = properties.clusterType
        if (clusterType != null) {
            val clusterManager = this.clusterManager(clusterType)
            if (clusterManager != null) {
                properties.clusterManager = clusterManager
            }
            // 判断是否使用集群模式
            if (properties.clusterManager != null) {
                // 以集群模式启动
                val future = CompletableFuture<Vertx>()
                Vertx.clusteredVertx(properties) { ar ->
                    if (ar.succeeded()) {
                        future.complete(ar.result())
                    } else {
                        future.completeExceptionally(ar.cause())
                    }
                }
                return future.get(properties.vertxTimeout, TimeUnit.SECONDS)
            }
        }

        // 非集群模式启动
        return Vertx.vertx(properties)
    }

    @Bean
    fun eventBus(vertx: Vertx): EventBus = vertx.eventBus()

    /**
     * 配置集群信息
     * @param clusterType 集群类型
     * */
    private fun clusterManager(clusterType: String): ClusterManager? {
        try {
            when (clusterType) {
                "hazelcast" -> {
                    val hazelcastClazz = Class
                            .forName("io.vertx.spi.cluster.hazelcast.HazelcastClusterManager")
                    val configClazz = Class
                            .forName("com.hazelcast.config.Config")
                    val loadConfig = configClazz.getMethod("load")
                    val hazelcastConfig = loadConfig.invoke(null)
                    return hazelcastClazz
                            .getConstructor(configClazz)
                            .newInstance(hazelcastConfig) as ClusterManager
                }
                "ignite" -> {
                    val igniteConfig = LoadFiles
                            .loadJsonFile("ignite.json", "classpath:ignite.json")
                    log.info("Ignite Config:$igniteConfig")
                    val igniteClazz = Class
                            .forName("io.vertx.spi.cluster.ignite.IgniteClusterManager")
                    return igniteClazz
                            .getConstructor(JsonObject::class.java)
                            .newInstance(igniteConfig) as ClusterManager
                }
                "zookeeper" -> {
                    val zkConfig = LoadFiles
                            .loadJsonFile("zookeeper.json", "classpath:zookeeper.json")
                    log.info("Zookeeper Config:$zkConfig")
                    val zookeeperClazz = Class
                            .forName("io.vertx.spi.cluster.zookeeper.ZookeeperClusterManager")
                    return zookeeperClazz
                            .getConstructor(JsonObject::class.java)
                            .newInstance(zkConfig) as ClusterManager
                }
            }
        } catch (e: Exception) {
            throw IllegalStateException("Failed to instantiate $clusterType", e)
        }
        return null
    }
}