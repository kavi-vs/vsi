package kavi.vsi.backend.utils

import io.vertx.core.json.JsonObject
import org.springframework.util.ResourceUtils
import java.io.*
import java.lang.StringBuilder
import kotlin.jvm.Throws

object LoadFiles {

    /**
     * 读取加载JSON文件
     * @param fileName 文件名
     * @return JSON类型
     * */
    fun loadJsonFile(fileName: String): JsonObject {
        val file = ResourceUtils.getFile(fileName)
        if (!file.exists()) {
            throw FileNotFoundException("File not found: $fileName")
        }
        return JsonObject(readFromInputStream(file.inputStream()))
    }

    /**
     * 按顺序读取加载JSON文件
     * @param args 文件名
     * @return JSON类型
     * */
    fun loadJsonFile(vararg args: String): JsonObject {
        listOf(*args).map { name ->
            val file = ResourceUtils.getFile(name)
            if (file.exists()) {
                return JsonObject(readFromInputStream(file.inputStream()))
            }
        }
        throw FileNotFoundException("File not found: $args")
    }

    /**
     * 读取数据流转字符类型
     * */
    @Throws(IOException::class)
    fun readFromInputStream(inputStream: InputStream): String {
        val resultStringBuilder = StringBuilder()
        BufferedReader(InputStreamReader(inputStream)).use { br ->
            var line: String?
            while (br.readLine().also { line = it } != null) {
                resultStringBuilder.append(line).append("\n")
            }
        }
        return resultStringBuilder.toString()
    }
}