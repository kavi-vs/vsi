package kavi.vsi.backend

import io.vertx.reactivex.core.Vertx
import kavi.vsi.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder

/**
 * SDK单独运行服务
 * */
@SpringBootApplication
class Application : CommandLineRunner {
    // 异步框架服务
    @Autowired
    private lateinit var vertx: Vertx

    @Autowired
    private lateinit var userRepository: UserRepository

    /**
     * Spring启动后执行
     * */
    override fun run(vararg args: String) {
        vertx.eventBus().consumer<String>("abc"){
            println(it.body())
        }
        userRepository.findByUses().subscribe({
            println(it)
        },{
            it.printStackTrace()
        })
    }

    /**
     * 启动器
     * */
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
             SpringApplicationBuilder(Application::class.java).run(*args)
        }
    }
}