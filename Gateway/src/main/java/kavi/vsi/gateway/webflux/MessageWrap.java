package kavi.vsi.gateway.webflux;

import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 消息封装
 * todo 消息字段补全
 * */
public class MessageWrap {

    // 返回结果
    Object body;

    // 消息
    String message;

    /**
     * 默认返回内容封装
     * @param body 请求内容
     * @param request 请求信息
     * @return 返回对象结果
     * */
    static Map<String, Object> build(Object body, ServerHttpRequest request) {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("timestamp", new Date());
        result.put("requestId", request.getId());
        result.put("data", body);
        return result;
    }
}
