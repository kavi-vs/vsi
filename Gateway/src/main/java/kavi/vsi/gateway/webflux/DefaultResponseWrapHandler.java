package kavi.vsi.gateway.webflux;

import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.CorePublisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 默认输出封装处理器
 * */
public class DefaultResponseWrapHandler implements ResponseWrapHandler {

    @Override
    public CorePublisher<Object> result(Mono<Object> body, ServerWebExchange exchange, HandlerResult result) {
        return body.map(it -> MessageWrap.build(it, exchange.getRequest()));
    }

    @Override
    public CorePublisher<Object> result(Flux<Object> body, ServerWebExchange exchange, HandlerResult result) {
        return this.result(body.collectList().cast(Object.class), exchange, result);
    }

}
