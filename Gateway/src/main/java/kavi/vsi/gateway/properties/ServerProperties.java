package kavi.vsi.gateway.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * 服务属性配置
 * */
@ConfigurationProperties(prefix = "server", ignoreUnknownFields = true)
public class ServerProperties {
    // 服务器访问端口
    private Integer port;

    // 静态路径
    private Map<String, String> statics = new HashMap<>();

    public void setStatics(Map<String, String> statics) {
        this.statics = statics;
    }

    public Map<String, String> getStatics() {
        return statics;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
