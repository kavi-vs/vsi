package kavi.vsi.gateway.exception;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;

/**
 * 系统全局异常消息错误属性
 * */
@Component
public class ServerErrorAttributes extends DefaultErrorAttributes {

    /**
     * 重定义错误属性
     * @param request 服务请求信息
     * @param options 指定错误输出属性
     * @return 返回错误Map值
     * */
    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        return super.getErrorAttributes(request, options);
    }
}
