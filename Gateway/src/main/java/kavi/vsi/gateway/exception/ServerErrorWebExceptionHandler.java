package kavi.vsi.gateway.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * 全局异常错误处理
 * */
@Component
@Order(-2) // 优先级别至高，否则将被DefaultErrorWebExceptionHandler拦截导致无法处理ResponseStatusException
public class ServerErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    public ServerErrorWebExceptionHandler(ServerErrorAttributes errorAttributes, ApplicationContext applicationContext,
                                          ServerCodecConfigurer serverCodecConfigurer) {
        super(errorAttributes, new ResourceProperties(), applicationContext);
        super.setMessageWriters(serverCodecConfigurer.getWriters());
        super.setMessageReaders(serverCodecConfigurer.getReaders());
    }

    /**
     * 定义全局返回的错误消息内容
     * */
    private ErrorAttributeOptions options = ErrorAttributeOptions.of(
            ErrorAttributeOptions.Include.MESSAGE, // 包含错误消描述
            ErrorAttributeOptions.Include.EXCEPTION // 包含异常类型
            // ErrorAttributeOptions.Include.STACK_TRACE // 包含Track信息
    );

    /**
     * 指定全局路由地址错误输出方法
     * */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(final ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    /**
     * 输出错误信息
     * */
    private Mono<ServerResponse> renderErrorResponse(final ServerRequest request) {
        final Map<String, Object> errorPropertiesMap = this.getErrorAttributes(request, options);
        Integer status = (Integer)errorPropertiesMap.getOrDefault("status", HttpStatus.BAD_GATEWAY.value());
        log.info("Error Attribute Map" + errorPropertiesMap);
        return ServerResponse.status(status)
                .contentType(MediaType.APPLICATION_JSON) // 以JSON格式输出
                .body(BodyInserters.fromValue(errorPropertiesMap));
    }
}