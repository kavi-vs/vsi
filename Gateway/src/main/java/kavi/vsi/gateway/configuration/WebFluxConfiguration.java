package kavi.vsi.gateway.configuration;


import kavi.vsi.gateway.properties.ServerProperties;
import kavi.vsi.gateway.webflux.WebFluxResponseBodyResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ReactiveAdapterRegistry;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.accept.RequestedContentTypeResolver;
import org.springframework.web.reactive.config.DelegatingWebFluxConfiguration;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.result.method.annotation.ResponseBodyResultHandler;

import java.util.Map;

/**
 * WebFlux 配置参数
 * */
@Configuration
@EnableConfigurationProperties({ ServerProperties.class })
public class WebFluxConfiguration extends DelegatingWebFluxConfiguration {

    /**
     * 服务配置
     * */
    @Autowired
    ServerProperties serverProperties;

    /**
     * 定义静态资源路径
     * */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        Map<String, String> statics = serverProperties.getStatics();
        if (!statics.isEmpty()) statics.forEach((key, value) -> {
            registry.addResourceHandler(key).addResourceLocations(value);
        });
        // registry.setCacheControl(CacheControl.maxAge(365, TimeUnit.DAYS));
    }

    /**
     * 重定义全局输出处理
     * */
    @Bean
    public ResponseBodyResultHandler responseBodyResultHandler(
            @Qualifier("webFluxAdapterRegistry") ReactiveAdapterRegistry reactiveAdapterRegistry,
            ServerCodecConfigurer serverCodecConfigurer,
            @Qualifier("webFluxContentTypeResolver") RequestedContentTypeResolver contentTypeResolver) {
        return new WebFluxResponseBodyResultHandler(
                serverCodecConfigurer().getWriters(),
                contentTypeResolver,
                reactiveAdapterRegistry);
    }
}