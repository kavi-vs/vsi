package kavi.vsi.gateway.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

/**
 *
 * 系统Bean初始化配置
 * */
@Configuration
@EnableR2dbcRepositories
public class BeanConfiguration {

}
