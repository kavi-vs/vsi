package kavi.vsi.gateway.authorization.repository;

import kavi.vsi.gateway.authorization.entity.AuthorityRelation;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * 用户角色权限关系存储过程
 * */
public interface AuthorityRelationRepository extends R2dbcRepository<AuthorityRelation, String> {

    /**
     * 根据父级查找子集
     * */
    @Query("SELECT child FROM " + AuthorityRelation.tableName + " WHERE parent in (:name) AND deleted_at = 0")
    Flux<String> findChildByParent(List<String> names);

}