package kavi.vsi.gateway.authorization.repository;

import kavi.vsi.gateway.authorization.entity.AuthorityAssignment;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

/**
 * 用户角色权限分配存储过程
 * */
public interface AuthorityAssignmentRepository extends R2dbcRepository<AuthorityAssignment, String> {

    @Query("SELECT * FROM " + AuthorityAssignment.tableName + " WHERE user_id = :userId")
    Flux<AuthorityAssignment> findByUserId(Long userId);
}