package kavi.vsi.gateway.authorization.repository;

import ht.ewx.gateway.authorization.entity.Authority;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

/**
 * 用户角色权限存储过程
 * */
public interface AuthorityRepository extends R2dbcRepository<Authority, String>{
}