package kavi.vsi.gateway.authorization;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限的配置
 * */
@ConfigurationProperties(prefix = "security", ignoreUnknownFields = true)
public class SecurityProperties {

    /**
     * 无需授权允许访问的URI
     * */
    private List<String> allowPaths = new ArrayList<>();

    public List<String> getAllowPaths() {
        return allowPaths;
    }

    public void setAllowPaths(List<String> allowPaths) {
        this.allowPaths = allowPaths;
    }
}
