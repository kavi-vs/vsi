package kavi.vsi.gateway.authorization.entity;

import org.springframework.data.relational.core.mapping.Table;

/**
 * 用户权限角色表
 * */
@Table(Authority.tableName)
public class Authority {
    /* 表名 */
     public final static String tableName = "system_authority";


    /* 权限名称 */
    private String name;
    /* 权限类型： 1为角色 2为权限 */
    private Integer type;
    /* 规则 */
    private String rule;
    /* 描述 */
    private String description;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }

    public String getRule() {
        return rule;
    }
    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}