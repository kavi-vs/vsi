package kavi.vsi.gateway.authorization.service;

import kavi.vsi.gateway.authorization.entity.AuthorityAssignment;
import kavi.vsi.gateway.authorization.repository.AuthorityAssignmentRepository;
import kavi.vsi.gateway.authorization.repository.AuthorityRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorizationService implements AuthorizationServiceInterface {

    // 权限角色关系表
    @Autowired
    AuthorityRelationRepository authorityRelationRepository;

    // 权限用户分配表
    @Autowired
    AuthorityAssignmentRepository authorityAssignmentRepository;

    /**
     * 根据用户ID获取所有分配的关联权限
     * */
    @Override
    public Mono<List<String>> authorizationByUserId(Long userId) {
        return authorityAssignmentRepository
                .findByUserId(userId)
                .map(AuthorityAssignment::getAuthority_name) // 获取权限名称
                .collectList()
                .flatMap(list -> this.recursion(new HashSet<>(list), list));
    }

    /**
     * 递归获取子项
     * */
    private Mono<List<String>> recursion (HashSet<String> result, List<String> condition) {
        return this.authorityRelationRepository.findChildByParent(condition)
                .collectList()
                .map(list -> list.stream().filter(it -> !result.contains(it)).collect(Collectors.toList())) // 过滤已存在
                .flatMap(it -> {
                    result.addAll(it); // 收集子项
                    return it.isEmpty() ? Mono.empty() : this.recursion(result, it); // 如果有子项执行递归累加
                })
                .defaultIfEmpty(result.stream().collect(ArrayList::new, List::add, List::addAll));
    }
}
